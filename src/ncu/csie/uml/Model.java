package ncu.csie.uml;

import java.awt.Point;
import java.awt.Shape;
import java.awt.geom.Area;
import java.util.ArrayList;
import java.util.List;

import ncu.csie.uml.objects.AssLine;
import ncu.csie.uml.objects.Class;
import ncu.csie.uml.objects.ComLine;
import ncu.csie.uml.objects.GenLine;
import ncu.csie.uml.objects.UMLBasic;
import ncu.csie.uml.objects.UMLLink;
import ncu.csie.uml.objects.UMLObject;
import ncu.csie.uml.objects.UseCase;

public class Model {
	
	private int width = 100;
	
	private List<UMLObject> objects; 	
	private List<ArrayList<UMLObject>> composites;
	
	public Model(){
		objects = new ArrayList<>();		
		composites = new ArrayList<ArrayList<UMLObject>>();
	}
		

	public void addUseCase(Point p){
		UMLObject usecase = new UseCase("", p.x, p.y, width, 50);
		int depth = checkDepth(usecase);
		usecase.setDepth(depth);
		objects.add(usecase);
	}
	
	public void addClass(Point p){
		UMLObject umlclass = new Class("", p.x, p.y, width, 80);
		int depth = checkDepth(umlclass);
		umlclass.setDepth(depth);
		objects.add(umlclass);
	}
	
	public void addAssLine(Point p1, Point p2){
		UMLObject assLine = new AssLine("", p1.x, p1.y, p2.x, p2.y);
		int depth = checkDepth(assLine);
		assLine.setDepth(depth);
		objects.add(assLine);
	}
	
	public void addGenLine(Point p1, Point p2){
		UMLObject genLine = new GenLine("", p1.x, p1.y, p2.x, p2.y);
		int depth = checkDepth(genLine);
		genLine.setDepth(depth);
		objects.add(genLine);
	}
	
	public void addComLine(Point p1, Point p2){
		UMLObject comline = new ComLine("", p1.x, p1.y, p2.x, p2.y);
		int depth = checkDepth(comline);
		comline.setDepth(depth);
		objects.add(comline);
	}
	
	
	public Point findUMLBasicClosestPoint(Point p){
		int topIndex = findTopUMLBasicObjectIndex(p);
		if(topIndex >= 0){
			UMLBasic umlbox = (UMLBasic)objects.get(topIndex);
			Point p2 = null;
			double min = 100000000;								
			if(p.distance(umlbox.getpUP()) < min){
				min = p.distance(umlbox.getpUP());
				p2 = umlbox.getpUP();
			}
			if(p.distance(umlbox.getpDown()) < min){
				min = p.distance(umlbox.getpDown());
				p2 = umlbox.getpDown();
			}
			if(p.distance(umlbox.getpLeft()) < min){
				min = p.distance(umlbox.getpLeft());
				p2 = umlbox.getpLeft();
			}
			if(p.distance(umlbox.getpRight()) < min){
				min = p.distance(umlbox.getpRight());
				p2 = umlbox.getpRight();
			}
				
			return p2;
		}
		
		return null;
	}		
	
	
	public List<UMLObject> getUMLObjects(){
		return objects;
	}
	
	private int checkDepth(UMLObject obj){
		
		int count = 0;
		for(UMLObject umlobj: objects){
			Area areaObj = new Area(obj.getBounds());
			areaObj.intersect(new Area(umlobj.getBounds()));
			if(!areaObj.isEmpty()){
				count--;
			}
		}
		return count;
	}

	public void selectSingleObject(Point p) {
		int topIndex = findTopUMLBasicObjectIndex(p);
		if(topIndex >= 0){
			objects.get(topIndex).setSelected(true);
		}		
	}
	
	public void resetSelect(){
		for(UMLObject obj: objects){
			obj.setSelected(false);
		}
	}


	public void selectMutiObject(Shape bounds) {
		
		for(int i=0; i<objects.size(); ++i){			
			Area areaBounds = new Area(bounds);			
			UMLObject obj = objects.get(i);
			Area objBounds = new Area(obj.getBounds());
			objBounds.subtract(areaBounds);
			if(obj instanceof UMLBasic && objBounds.isEmpty()){
				if(obj.isSelected()){
					obj.setSelected(false);
				}
				else{
					obj.setSelected(true);
				}
			}
		}
	}


	public void groupSelectUMLObject() {		
		ArrayList<UMLObject> tmpComp = new ArrayList<>();
		for(UMLObject obj: objects){
			if(obj.isSelected()){
				tmpComp.add(obj);
			}
		}		
		composites.add(tmpComp);		
	}


	public void ungroupSelectUMLObject() {
		if(composites.size() > 0){
			composites.remove(composites.size()-1);
		}
		
	}

	public void moveUMLBasicObject(Point p0, Point p1) {
		int topIndex = findTopUMLBasicObjectIndex(p0);
		if(topIndex >= 0){
			UMLBasic umlbasic = (UMLBasic) objects.get(topIndex);
			int x = (int) (p1.getX() -(p0.getX() - umlbasic.getX()));
			int y = (int) (p1.getY() - (p0.getY()-umlbasic.getY()));
			umlbasic.setX(x);
			umlbasic.setY(y);			
			umlbasic.createBound();
			int depth = checkDepth(umlbasic);
			umlbasic.setDepth(depth);
			Point pOldUp, pOldDown, pOldLeft, pOldRight;
			pOldUp = umlbasic.getpUP();
			pOldDown = umlbasic.getpDown();
			pOldLeft = umlbasic.getpLeft();
			pOldRight = umlbasic.getpRight();
			umlbasic.createLinkPort();
			moveUMLLinkObject(pOldUp, umlbasic.getpUP());
			moveUMLLinkObject(pOldDown, umlbasic.getpDown());
			moveUMLLinkObject(pOldLeft, umlbasic.getpLeft());
			moveUMLLinkObject(pOldRight, umlbasic.getpRight());
			if(umlbasic instanceof Class)
				((Class)umlbasic).createBar();
			objects.set(topIndex, umlbasic);
			
		}
		
	}
	
	private void moveUMLLinkObject(Point pOld, Point pNew) {
		for(int i=0; i<objects.size(); ++i){
			if(objects.get(i) instanceof UMLLink){
				UMLLink umllink = (UMLLink)objects.get(i);
				if(umllink.getStartPoint().distance(pOld) == 0){
					umllink.setStartPoint(pNew);
				}
				else if(umllink.getEndPoint().distance(pOld) == 0){
					umllink.setEndPoint(pNew);
				}
				umllink.createBound();
				umllink.createArrow();				
				objects.set(i, umllink);
			}
		}
		
	}


	public int findTopUMLBasicObjectIndex(Point p){
		int minDepth = 1;
		int key = -1;
		for(int i=0; i<objects.size(); ++i){
			UMLObject obj = objects.get(i);
			if(obj instanceof UMLBasic && obj.getBounds().contains(p) && obj.getDepth() < minDepth){
				minDepth = obj.getDepth();
				key = i;
			}
		}
		return key;
	}
	
	public void removeUMLObject(UMLObject obj){
		System.out.println(objects.remove(obj));
	}


	public void renameUMLBasicObject(String name) {
		for(UMLObject obj: objects){
			if(obj.isSelected()){
				obj.setName(name);
				return ;
			}
		}		
	}


	public List<ArrayList<UMLObject>> getComposites() {
		return composites;
	}
}
