package ncu.csie.uml.objects;

import java.awt.Graphics2D;
import java.awt.geom.Ellipse2D;

public class UseCase extends UMLBasic{

	public UseCase(String name, int x, int y, int width, int height) {
		super(name, x, y, width, height);
		createBound();
	}

	@Override
	public void createBound() {
		bounds = new Ellipse2D.Float(x, y, width, height);		
	}

	@Override
	public void render(Graphics2D g2d) {
		super.render(g2d);
		g2d.drawString(name, x+width/2-5, y+height/2+5);									
		g2d.draw(bounds);
		
	}
}
