package ncu.csie.uml.objects;

import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.awt.geom.Path2D;

public class GenLine extends UMLLink {

		
	
	public GenLine(String name, int x, int y, int x2, int y2) {
		super(name, x, y, x2, y2);
		createArrow();
		
	}

	
	
	
	
	@Override
	public void createArrow() {				
		super.createArrow();
		
		PointyTriangle pointyTriangle = new PointyTriangle();
		
		double rotation = 0f;
                        
        int startX = x;
        int startY = y;
        int endX = endPoint.x;
        int endY = endPoint.y;
        int deltaX = endX - startX;
        int deltaY = endY - startY;

        rotation = -Math.atan2(deltaX, deltaY);
        rotation = Math.toDegrees(rotation) + 180;
        
        Rectangle bounds = pointyTriangle.getBounds();

        
        AffineTransform at = new AffineTransform();

        at.translate(endX - (bounds.width / 2), endY - (bounds.height / 2));
        at.rotate(Math.toRadians(rotation), bounds.width / 2, bounds.height / 2);
        arrow = new Path2D.Float(pointyTriangle, at);
	}





	@Override
	public void render(Graphics2D g2d) {		
		super.render(g2d);
		g2d.draw(arrow);
	}
	
	
	
	

}
