package ncu.csie.uml.objects;

import java.awt.geom.Path2D;

public class PointyDiamond extends Path2D.Float{
	
	public PointyDiamond(){
		moveTo(15, 0);
        lineTo(30, 15);
        lineTo(15,30);
        lineTo(0, 15);        
        lineTo(15, 0);
	}
}
