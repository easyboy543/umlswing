package ncu.csie.uml.objects;

import java.awt.Graphics2D;
import java.awt.Point;

public abstract class UMLBasic extends UMLObject{

	protected int width, height;
	protected Point pUP, pDown, pLeft, pRight;
	public UMLBasic(String name, int x, int y, int width, int height) {
		super(name, x, y);
		this.name = name;
		this.width = width;
		this.height = height;
		createLinkPort();
	}	
	public void createLinkPort() {
		pUP = new Point(x+width/2, y);
		pDown = new Point(x+width/2, y+height);
		pLeft = new Point(x, y+height/2);
		pRight = new Point(x+width, y+height/2);
	}
	public int getWidth() {
		return width;
	}
	public void setWidth(int width) {
		this.width = width;
	}
	public int getHeight() {
		return height;
	}
	public void setHeigth(int height) {
		this.height = height;
	}
	public Point getpUP() {
		return pUP;
	}
	public void setpUP(Point pUP) {
		this.pUP = pUP;
	}
	public Point getpDown() {
		return pDown;
	}
	public void setpDown(Point pDown) {
		this.pDown = pDown;
	}
	public Point getpLeft() {
		return pLeft;
	}
	public void setpLeft(Point pLeft) {
		this.pLeft = pLeft;
	}
	public Point getpRight() {
		return pRight;
	}
	public void setpRight(Point pRight) {
		this.pRight = pRight;
	}
	
	
	
	@Override
	public void render(Graphics2D g2d) {
		if(isSelected){
			g2d.fillRect(pUP.x, pUP.y, 5, 5);
			g2d.fillRect(pDown.x, pDown.y, 5, 5);
			g2d.fillRect(pLeft.x, pLeft.y, 5, 5);
			g2d.fillRect(pRight.x, pRight.y, 5, 5);			
		}		
	}
	
	

}
