package ncu.csie.uml.objects;

import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;

public class Class extends UMLBasic{

	protected Point pUP1, pUP2, pDown1, pDown2;
	protected int distance;
	public Class(String name, int x, int y, int width, int height) {
		super(name, x, y, width, height);
		createBound();
		createBar();
	}
	public void createBar(){
		distance = height/3;
		pUP1 = new Point(x, y+distance*1);
		pUP2 = new Point(x+width,y+distance*1);
		pDown1 = new Point(x,y+distance*2);
		pDown2 = new Point(x+width,y+distance*2);
	}
	public Point getpUP1() {
		return pUP1;
	}
	public void setpUP1(Point pUP1) {
		this.pUP1 = pUP1;
	}
	public Point getpUP2() {
		return pUP2;
	}
	public void setpUP2(Point pUP2) {
		this.pUP2 = pUP2;
	}
	public Point getpDown1() {
		return pDown1;
	}
	public void setpDown1(Point pDown1) {
		this.pDown1 = pDown1;
	}
	public Point getpDown2() {
		return pDown2;
	}
	public void setpDown2(Point pDown2) {
		this.pDown2 = pDown2;
	}
	@Override
	public void createBound() {
		bounds = new Rectangle(x, y, width, height);		
	}
	@Override
	public void render(Graphics2D g2d) {
		super.render(g2d);
		g2d.drawString(name, x+23, y+15);									
		g2d.draw(bounds);
		g2d.drawLine(pUP1.x, pUP1.y, pUP2.x, pUP2.y);
		g2d.drawLine(pDown1.x, pDown1.y, pDown2.x, pDown2.y);		
	}
	
}
