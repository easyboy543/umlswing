package ncu.csie.uml.objects;

import java.awt.Graphics2D;
import java.awt.Shape;

public abstract class UMLObject {
		
	protected String name;
	protected int x, y;
	protected int depth;
	protected Shape bounds;
	protected boolean isSelected;
	public UMLObject(String name, int x, int y){
		this.name = name;
		this.x = x;
		this.y = y;
		isSelected = false;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getX() {
		return x;
	}
	public void setX(int x) {
		this.x = x;
	}
	public int getY() {
		return y;
	}
	public void setY(int y) {
		this.y = y;
	}
	public int getDepth() {
		return depth;
	}
	public void setDepth(int depth) {
		this.depth = depth;
	}
	public Shape getBounds() {
		return bounds;
	}
	public void setBounds(Shape bounds) {
		this.bounds = bounds;
	}
	public boolean isSelected() {
		return isSelected;
	}
	public void setSelected(boolean isSelected) {
		this.isSelected = isSelected;
	}
	
	public abstract void createBound();
	public abstract void render(Graphics2D g2d);
	
}
