package ncu.csie.uml.objects;

import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Shape;
import java.awt.geom.Line2D;

public class UMLLink extends UMLObject{

	protected Point startPoint, endPoint;
	protected Shape arrow;
	
	public UMLLink(String name, int x, int y, int x2, int y2) {
		super(name, x, y);
		startPoint = new Point(x, y);
		endPoint = new Point(x2, y2);
		createBound();
	}
	public Point getStartPoint() {
		return startPoint;
	}
	public void setStartPoint(Point startPoint) {
		this.startPoint = startPoint;
	}
	public Point getEndPoint() {
		return endPoint;
	}
	public void setEndPoint(Point endPoint) {
		this.endPoint = endPoint;
	}
	@Override
	public void render(Graphics2D g2d) {
		g2d.draw(bounds);		
	}
	@Override
	public void createBound() {		
		bounds = new Line2D.Float(startPoint, endPoint);
	}
	
	public void createArrow(){
		
	}
	
}
