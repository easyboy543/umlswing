package ncu.csie.uml;

import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.geom.Rectangle2D;
import java.util.List;
import java.util.Locale;

import javax.swing.JOptionPane;

import ncu.csie.uml.objects.AssLine;
import ncu.csie.uml.objects.ComLine;
import ncu.csie.uml.objects.GenLine;
import ncu.csie.uml.objects.UMLBasic;
import ncu.csie.uml.objects.UMLLink;
import ncu.csie.uml.objects.UMLObject;

public class Controller {
	
	/*
	 *    select => mode = 1
	 *    assline => mode = 2
	 *    genline => mode = 3
	 *    comline => mode = 4
	 *    class => mode = 5
	 *    usecase => mode = 6
	 *    
	 */
		
	
	private Model model;
	private View view;
	private ActionListener actionListener;
	private MouseListener mouseListener;	
	private MouseMotionListener mouseMotionListener;
	private Point startPoint, endPoint;	
	
	public Controller(Model model, View view){
		this.model = model;
		this.view = view;
	}
	
	public void control(){
		mouseListener = new MouseListener() {
			
			@Override
			public void mouseReleased(MouseEvent e) {
				if(e.getButton() != MouseEvent.BUTTON1)
					return ;							
				if(view.getDrawMode() == 5 || view.getDrawMode() == 6)
					return ;				
				
				view.getPanelEdit().update(view.getPanelEdit().getGraphics());
				draw();
				
				if(view.getDrawMode() == 1){
					model.resetSelect();
					int minX = Math.min(e.getX(), startPoint.x);
				    int minY = Math.min(e.getY(), startPoint.y);
				    int maxX = Math.max(e.getX(), startPoint.x);
				    int maxY = Math.max(e.getY(), startPoint.y);
				    int x = minX;
				    int y = minY;
				    int width = maxX - minX;
				    int height = maxY - minY;
				    Rectangle2D bounds = new Rectangle2D.Double(x, y, width, height);
				    model.selectMutiObject(bounds);	
				    view.getPanelEdit().update(view.getPanelEdit().getGraphics());
				    drawGroupArea();	
				}
				else{													
					endPoint = model.findUMLBasicClosestPoint(endPoint);
					if(endPoint == null)
						return ;
										
					if(view.getDrawMode() == 2)
						model.addAssLine(startPoint, endPoint);
					if(view.getDrawMode() == 3)
						model.addGenLine(startPoint, endPoint);
					if(view.getDrawMode() == 4)
						model.addComLine(startPoint, endPoint);				
					view.getPanelEdit().update(view.getPanelEdit().getGraphics());						
				}
				draw();
			}
			
			@Override
			public void mousePressed(MouseEvent e) {
				if(view.getDrawMode() == 5 || view.getDrawMode() == 6)
					return ;			
				if(view.getDrawMode() > 1 && view.getDrawMode() < 5)
					startPoint = model.findUMLBasicClosestPoint(e.getPoint());
				if(view.getDrawMode() == 1)
					startPoint = e.getPoint();	
				draw();
			}
			
			@Override
			public void mouseExited(MouseEvent e) {
				
				
			}
			
			@Override
			public void mouseEntered(MouseEvent e) {
				
				
			}
			
			@Override
			public void mouseClicked(MouseEvent e) {
				if(e.getButton() != MouseEvent.BUTTON1)
					return ;				
				if(view.getDrawMode() == 1){										
					selectSingleObject(e.getPoint());
				}
				if(view.getDrawMode() == 5){
					model.addClass(e.getPoint());
				}
				if(view.getDrawMode() == 6){
					model.addUseCase(e.getPoint());
				}
				draw();
			}
		};		
		mouseMotionListener = new MouseMotionListener() {
			
			@Override
			public void mouseMoved(MouseEvent e) {
								
			}
			
			@Override
			public void mouseDragged(MouseEvent e) {
				if(e.getButton() == MouseEvent.BUTTON1)
					return;								
				if(view.getDrawMode() == 5 || view.getDrawMode() == 6)
					return ;				
				if(startPoint == null)
					return ;
				Graphics2D g2d = (Graphics2D)view.getPanelEdit().getGraphics();
				if(view.getDrawMode() == 1){
					// start point does not contain in shape					
					if(model.findUMLBasicClosestPoint(startPoint) == null){
						//z g2d = (Graphics2D)view.getPanelEdit().getGraphics();
						int minX = Math.min(e.getX(), startPoint.x);
					    int minY = Math.min(e.getY(), startPoint.y);
					    int maxX = Math.max(e.getX(), startPoint.x);
					    int maxY = Math.max(e.getY(), startPoint.y);
	
					    int x = minX;
					    int y = minY;
					    int width = maxX - minX;
					    int height = maxY - minY;
						view.getPanelEdit().update(g2d);
						g2d.drawRect(x, y, width, height);
						drawGroupArea();					
					}
					else{
						view.getPanelEdit().update(g2d);
						model.moveUMLBasicObject(startPoint, e.getPoint());
						startPoint = e.getPoint();
					}
				}
				else{
									
					UMLLink umllink = null;
					//Graphics2D g2d = (Graphics2D)view.getPanelEdit().getGraphics();
					if(view.getDrawMode() == 2)
						umllink = new AssLine("", startPoint.x, startPoint.y, e.getX(), e.getY());
					if(view.getDrawMode() == 3)
						umllink = new GenLine("", startPoint.x, startPoint.y, e.getX(), e.getY());
					if(view.getDrawMode() == 4)
						umllink = new ComLine("", startPoint.x, startPoint.y, e.getX(), e.getY());
					
					endPoint = e.getPoint();
					view.getPanelEdit().update(g2d);
					umllink.render(g2d);
				
				}
				draw();			
			}
		};
		actionListener = new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				if(e.getActionCommand().toLowerCase().equals("group")){
					groupSelectUMLObject();
				}
				else if(e.getActionCommand().toLowerCase().equals("ungroup")){
					ungroupSelectUMLObject();
				}
				else if(e.getActionCommand().toLowerCase().equals("rename")){
					JOptionPane.setDefaultLocale(Locale.ENGLISH);
					String name = JOptionPane.showInputDialog(null, "Please enter a input name", "Rename",
							JOptionPane.PLAIN_MESSAGE);  
					if(view.getDrawMode() == 1 && name != null){
						model.renameUMLBasicObject(name);
						view.getPanelEdit().update(view.getPanelEdit().getGraphics());
						draw();
					}
				}
				else{					
					view.getPanelEdit().update(view.getPanelEdit().getGraphics());
					model.resetSelect();
					draw();
				}
				
				
			}
		};
		
		view.getBtnAssLine().addActionListener(actionListener);
		view.getBtnGenLine().addActionListener(actionListener);
		view.getBtnComLine().addActionListener(actionListener);
		view.getBtnClass().addActionListener(actionListener);
		view.getBtnUseCase().addActionListener(actionListener);
		
		view.getPanelEdit().addMouseListener(mouseListener);
		view.getPanelEdit().addMouseMotionListener(mouseMotionListener);
		view.getMnEditItemGroup().addActionListener(actionListener);
		view.getMnEditItemUnGroup().addActionListener(actionListener);
		view.getMnEditItemRename().addActionListener(actionListener);
	}
		

	protected void ungroupSelectUMLObject() {
		model.ungroupSelectUMLObject();
		view.getPanelEdit().update(view.getPanelEdit().getGraphics());
		draw();
		drawGroupArea();
	}

	private void groupSelectUMLObject() {		
		model.groupSelectUMLObject();
		view.getPanelEdit().update(view.getPanelEdit().getGraphics());
		draw();
		drawGroupArea();
	}	

	private void selectSingleObject(Point p){		
		
		model.resetSelect();
		view.getPanelEdit().update(view.getPanelEdit().getGraphics());				
		model.selectSingleObject(p);
		draw();		
		if(model.findUMLBasicClosestPoint(p) != null){
			drawGroupArea();
		}
	}
			
			
	private void draw(){	
		Graphics2D g2d = (Graphics2D)view.getPanelEdit().getGraphics();			
		for(UMLObject obj: model.getUMLObjects()){
			obj.render(g2d);											
		}		
	}
	
	private void drawGroupArea(){
		Graphics2D g2d = (Graphics2D)view.getPanelEdit().getGraphics();
		for(List<UMLObject> listuml: model.getComposites()){
			if(listuml.size() > 0){
				UMLBasic umlbasic = (UMLBasic) listuml.get(0);
				int minX, minY, maxX, maxY;
				minX = umlbasic.getX();
				minY = umlbasic.getY();
				maxX = umlbasic.getX()+umlbasic.getWidth();
				maxY = umlbasic.getY()+umlbasic.getHeight();
				for(int i=1; i<listuml.size(); ++i){
					umlbasic = (UMLBasic) listuml.get(i);
					minX = Math.min(minX, umlbasic.getX());
					minY = Math.min(minY, umlbasic.getY());
					maxX = Math.max(maxX, umlbasic.getX()+umlbasic.getWidth());
					maxY = Math.max(maxY, umlbasic.getY()+umlbasic.getHeight());
				}				
				int x = minX;
			    int y = minY;
			    int width = maxX - minX;
			    int height = maxY - minY;				
				g2d.drawRect(x, y, width, height);				
			}
		}
	}
}
