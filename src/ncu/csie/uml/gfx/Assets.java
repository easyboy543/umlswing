package ncu.csie.uml.gfx;

import javax.swing.ImageIcon;

import ncu.csie.uml.View;

public class Assets {
	public static ImageIcon btnSelect, btnSelected, btnAssLine, btnAssLined, btnGenLine, btnGenLined, btnComLine, btnComLined,
								btnClass, btnClassed, btnUseCase, btnUseCased;
	
	public static void init(){
		btnSelect = new ImageIcon(View.class.getResource("/textures/btnSelect.PNG"));
		btnSelected = new ImageIcon(View.class.getResource("/textures/btnSelected.PNG"));
		
		btnAssLine = new ImageIcon(View.class.getResource("/textures/btnAssLine.PNG"));
		btnAssLined = new ImageIcon(View.class.getResource("/textures/btnAssLined.PNG"));
		
		btnGenLine = new ImageIcon(View.class.getResource("/textures/btnGenLine.PNG"));
		btnGenLined = new ImageIcon(View.class.getResource("/textures/btnGenLined.PNG"));
		
		btnComLine = new ImageIcon(View.class.getResource("/textures/btnComLine.PNG"));
		btnComLined= new ImageIcon(View.class.getResource("/textures/btnComLined.PNG"));
		
		btnClass  = new ImageIcon(View.class.getResource("/textures/btnClass.PNG"));
		btnClassed  = new ImageIcon(View.class.getResource("/textures/btnClassed.PNG"));
		
		btnUseCase =  new ImageIcon(View.class.getResource("/textures/btnUseCase.PNG"));
		btnUseCased =  new ImageIcon(View.class.getResource("/textures/btnUseCased.PNG"));
	}
}
