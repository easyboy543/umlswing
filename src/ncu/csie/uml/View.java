package ncu.csie.uml;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;

import ncu.csie.uml.gfx.Assets;

public class View {

	private JFrame frame;			
	private JButton btnGenLine;
	private JButton btnSelect;
	private JButton btnUseCase;
	private JButton btnAssLine;
	private JButton btnComLine;
	private JButton btnClass;
	private JPanel panelEdit;
	
	private int drawMode = 0;
	private JMenu mnEdit;
	private JMenuItem mnEditItemGroup, mnEditItemUnGroup, mnEditItemRename;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					View window = new View();
					window.frame.setVisible(true);
					Model model = new Model();
					Controller contorller = new Controller(model, window);
					contorller.control();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public View() {
		Assets.init();
		initialize();
		
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();		
		frame.setResizable(false);
		frame.setBounds(100, 100, 1000, 900);
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		panelEdit = new JPanel();		
		panelEdit.setBounds(146, 0, 848, 850);
		frame.getContentPane().add(panelEdit);
		
		btnSelect = new JButton("");		
		btnSelect.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				btnReset();
				btnSelect.setIcon(Assets.btnSelected);
				drawMode = 1;
			}
		});
		btnSelect.setIcon(Assets.btnSelect);		
		
		btnSelect.setBounds(10, 303, 128, 111);
		frame.getContentPane().add(btnSelect);
		
		btnAssLine = new JButton("");		
		btnAssLine.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnReset();
				btnAssLine.setIcon(Assets.btnAssLined);	
				drawMode = 2;
			}
		});
		btnAssLine.setIcon(Assets.btnAssLine);
		btnAssLine.setBounds(10, 716, 128, 111);
		frame.getContentPane().add(btnAssLine);
		
		btnGenLine = new JButton("");				
		btnGenLine.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnReset();
				btnGenLine.setIcon(Assets.btnGenLined);	
				drawMode = 3;
			}
		});
		btnGenLine.setIcon(Assets.btnGenLine);
		btnGenLine.setBounds(10, 582, 128, 111);
		frame.getContentPane().add(btnGenLine);
		
		btnComLine = new JButton("");		
		btnComLine.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnReset();
				btnComLine.setIcon(Assets.btnComLined);
				drawMode = 4;
			}
		});
		btnComLine.setIcon(Assets.btnComLine);
		btnComLine.setBounds(10, 443, 128, 111);
		frame.getContentPane().add(btnComLine);
		
		btnClass = new JButton("");		
		btnClass.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnReset();
				btnClass.setIcon(Assets.btnClassed);
				drawMode = 5;
			}
		});
		btnClass.setIcon(Assets.btnClass);
		btnClass.setBounds(10, 165, 128, 111);
		frame.getContentPane().add(btnClass);
		
		btnUseCase = new JButton("");		
		btnUseCase.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnReset();
				btnUseCase.setIcon(Assets.btnUseCased);	
				drawMode = 6;
			}
		});
		btnUseCase.setIcon(Assets.btnUseCase);
		btnUseCase.setBounds(10, 25, 128, 111);
		frame.getContentPane().add(btnUseCase);
		
		JMenuBar menuBar = new JMenuBar();
		frame.setJMenuBar(menuBar);
		
		JMenu mnFile = new JMenu("File");
		menuBar.add(mnFile);
		
		mnEdit = new JMenu("Edit");
		menuBar.add(mnEdit);
		
		mnEditItemGroup = new JMenuItem("Group");
		mnEdit.add(mnEditItemGroup);
		
		mnEditItemUnGroup = new JMenuItem("UnGroup");
		mnEdit.add(mnEditItemUnGroup);
		
		mnEditItemRename = new JMenuItem("Rename");
		mnEdit.add(mnEditItemRename);
				
	}
	
	private void btnReset(){
		getBtnSelect().setIcon(Assets.btnSelect);
		getBtnAssLine().setIcon(Assets.btnAssLine);
		getBtnGenLine().setIcon(Assets.btnGenLine);
		getBtnComLine().setIcon(Assets.btnComLine);
		getBtnClass().setIcon(Assets.btnClass);
		getBtnUseCase().setIcon(Assets.btnUseCase);
	}
		
	
	public JButton getBtnGenLine() {
		return btnGenLine;
	}
	public JButton getBtnSelect() {
		return btnSelect;
	}
	public JButton getBtnUseCase() {
		return btnUseCase;
	}
	public JButton getBtnAssLine() {
		return btnAssLine;
	}
	public JButton getBtnComLine() {
		return btnComLine;
	}
	public JButton getBtnClass() {
		return btnClass;
	}
	public JPanel getPanelEdit() {
		return panelEdit;
	}

	public int getDrawMode() {
		return drawMode;
	}

	public JMenuItem getMnEditItemGroup() {
		return mnEditItemGroup;
	}

	public JMenuItem getMnEditItemUnGroup() {
		return mnEditItemUnGroup;
	}
	
	public JMenuItem getMnEditItemRename(){
		return mnEditItemRename;
	}
		
}
